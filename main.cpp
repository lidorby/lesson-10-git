#include"Customer.h"
#include<map>

int printItems(Item _items[]);
int printMenu();
int main()
{
	int itemChoice = 1;
	int maxPrice = 0;
	int optionToDo = 0;
	Customer biggest;
	Customer curr;
	string name;
	map<string, Customer>::iterator it;
	map<string, Customer> abcCustomers;

	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	int choice = printMenu();
	while (choice != 4)
	{
		if (choice == 1)//creates new customer
		{
			cout << "Enter name:";//gets name
			cin.clear();
			cin >> name;
			it= abcCustomers.find(name);
			system("cls");
			if (it != abcCustomers.end())//checks if the name used
			{
				cout << "used user! returned to menu!" << endl;
			}
			else//name not used
			{
				itemChoice = 1;
				cin.clear();
				curr = Customer(name);
				while (itemChoice != 0)//gets item
				{
					cout << "The items you can buy are: (0 to exit)" << endl;
					itemChoice = printItems(itemList);
					if (itemChoice != 0)
					{
						curr.addItem(itemList[itemChoice - 1]);
					}
					cout << endl << endl;
					system("cls");
				}
				abcCustomers[name] = curr;
			}
		}
		else if (choice == 2)//uses current user
		{
			cout << "Enter name:";//gets name
			cin.clear();
			cin >> name;
			it = abcCustomers.find(name);
			if (it == abcCustomers.end())//checks if the name used
			{
				cout << "user not used! returned to menu!" << endl;
			}
			else//name used
			{
				cout << "1.Add items" << endl << "2.Remove items" << endl << "3.Back to menu" << endl;
				cin >> optionToDo;
				if (optionToDo == 3)//exit
				{
					break;
				}
				//add item
				else if (optionToDo == 1)
				{
					itemChoice = 1;
					cin.clear();
					curr = abcCustomers[name];
					while (itemChoice != 0)//gets item
					{
						cout << "The items you can buy are: (0 to exit)" << endl;
						itemChoice = printItems(itemList);
						system("cls");
						if (itemChoice != 0)
						{
							curr.addItem(itemList[itemChoice - 1]);
						}
						cout << "item added"<<endl;
					}
					abcCustomers.erase(name);
					abcCustomers[name] = curr;
				}
				//remove item
				else if (optionToDo == 2)
				{
					itemChoice = 1;
					curr = abcCustomers[name];
					while (itemChoice != 0)//prints the customer items, and total items
					{
						curr.PrintItems();
						cout << "list of total items:";
						itemChoice = printItems(itemList);
						system("cls");
						if (itemChoice != 0)
						{
							curr.removeItem(itemList[itemChoice - 1]);
						}
						cout << "item removed!" << endl;
					}
					abcCustomers.erase(name);
					abcCustomers[name] = curr;
				}

			}
		}
		else if (choice == 3)
		{
			maxPrice = 0;
			for (it = abcCustomers.begin(); it != abcCustomers.end(); it++)
			{
				if (it->second.totalSum() > maxPrice)
				{
					maxPrice = it->second.totalSum();
					biggest = it->second;
				}
			}
			cout << "biggest user was " << biggest.getName() << " and his total price is:" << maxPrice << endl;
		}
		cin.clear();
		choice = printMenu();
	}

	return 0;
}

int printMenu()
{
	int choice = 0;
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1.to sign as customer and buy items" << endl;
	cout << "2.to uptade existing customer's items" << endl;
	cout<<"3.to print the customer who pays the most" << endl;
	cout<<"4.to exit" << endl;
	cin >> choice;
	return choice;
}
int printItems(Item _items[])
{
	int i = 0, choice = 0;

	for (i = 0; i < 10; i++)
	{
		cout << (i+1)<<"  "<< _items[i].getName()<<"    price:" << _items[i].getPrice() <<endl;
	}
	cout << "What item would you like to Choose? Input:";
	cin >> choice;
	return choice;
}