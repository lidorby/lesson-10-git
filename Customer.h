#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item toAdd);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions
	void PrintItems();
	string getName() { return _name; };
private:
	string _name;
	set<Item> _items;


};
