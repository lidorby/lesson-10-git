#include "Item.h"

Item::Item(string name, string serial, double price)
{
	_name = name;
	_serialNumber = serial;
	_unitPrice = price;
	_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return _count*_unitPrice;
}

bool Item::operator<(const Item & other) const
{
	string _other = other.getSerial();
	if (_serialNumber < _other)
	{
		return true;
	}
	return false;
}

bool Item::operator>(const Item & other) const
{
	string _other = other.getSerial();
	if (_serialNumber > _other)
	{
		return true;
	}
	return false;
}

bool Item::operator==(const Item & other) const
{
	string _other = other.getSerial();
	if (_serialNumber == _other)
	{
		return true;
	}
	return false;
}

string Item::getName() const
{
	return _name;
}

string Item::getSerial() const
{
	return this->_serialNumber;
}

int Item::getCount () const
{
	return _count;
}

int Item::getPrice () const
{
	return _unitPrice;
}

void Item::incCount()
{
	_count++;
}

void Item::decCount()
{
	_count--;
}
