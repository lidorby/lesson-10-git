#include "Customer.h"

Customer::Customer(string name)
{
	_name = name;
}

Customer::Customer()
{
}

double Customer::totalSum() const
{
	double counter = 0;

	for (set<Item>::iterator it = _items.begin(); it != _items.end(); it++)
	{
		counter += it->totalPrice();
	}
	return counter;
}

void Customer::addItem(Item toAdd)
{

	set<Item>::iterator it = _items.find(toAdd);
	if (it == _items.end())
	{
		_items.insert(toAdd);
	}
	else
	{
		Item temp = *it;
		temp.incCount();
		_items.erase(it);
		_items.insert(temp);
	}
}

void Customer::removeItem(Item toAdd)
{
	set<Item>::iterator it = _items.find(toAdd);
	if (it == _items.end())
	{
		cout << "error, item not found";
	}
	else
	{
		if (it->getCount() == 1)
		{
			_items.erase(it);
		}
		else
		{
			Item temp = *it;
			temp.decCount();
			_items.erase(it);
			_items.insert(temp);
		}
	}
}

void Customer::PrintItems()
{
	cout << _name << "'s items"<< endl;
	for (set<Item>::iterator it = _items.begin(); it != _items.end(); it++)
	{
		cout << it->getCount() << " times " << it->getName() << ". price =" << it->getPrice()<< endl;
	}
}
